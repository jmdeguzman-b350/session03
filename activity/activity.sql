/*1. Create a s03 and activity folder and then create a file named solution.sql and write the SQL code necessary to insert the following records in the tables and to perform the following query.
2. Add the following records to the blog_db database:
    - Check User Image
    - Check Posts Image
3. Get all the post with an Author ID of 1.
4. Get all the user's email and datetime of creation.
5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
6. Delete the user with an email of "johndoe@gmail.com".
7. Push your solution in your gitlab repository and linked it to your Boodle.*/

-- inserting single users

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", 20210101010000);

-- inserting multiple users

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", 20210101020000), ("janesmith@gmail.com", "passwordC", 20210101030000), ("mariadelacruz@gmail.com", "passwordD", 20210101040000), ("johndoe@gmail.com", "passwordE", 20210101050000);


--inserting a posts

INSERT INTO posts (author_id, title, content, datetime_created) VALUES (1, "First Code", "Hello World!", 20210102010000);

--insert multiple Posts

INSERT INTO posts (author_id, title, content, datetime_created) VALUES 
(1, "Second Code", "Hello Earth!", 20210102020000),
(2, "Third Code", "Welcome to mars!", 20210102020000),
(4, "Fourth Code", "Bye bye solar system!", 20210102020000) ;


--Get all the post with an Author ID of 1.

SELECT * FROM posts WHERE author_id = 1;

--4. Get all the user's email and datetime of creation.

SELECT email, datetime_created FROM users;

--5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 5;

--6. Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users WHERE email = "johndoe@gmail.com";



--stress goals

INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("johndoe", "john1234", "John Doe", 09123456789, "john@mail.com", "Quezon City");

INSERT INTO playlists (datetime_created, user_id) VALUES (20230920080000, 1);

INSERT INTO playlists_songs (playlists_id, song_id) VALUES ();